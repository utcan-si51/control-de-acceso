-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-02-2018 a las 20:20:05
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `registroses`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `matricula` int(11) NOT NULL,
  `passwor` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`matricula`, `passwor`) VALUES
(16391039, '12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `idEmpleado` int(11) NOT NULL,
  `Numcolab` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `categoria` varchar(50) NOT NULL,
  `curp` varchar(30) NOT NULL,
  `sexo` varchar(20) NOT NULL,
  `correop` varchar(50) NOT NULL,
  `correoi` varchar(50) NOT NULL,
  `contra` varchar(30) NOT NULL,
  `numcel` varchar(30) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `caracteristicas` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`idEmpleado`, `Numcolab`, `nombre`, `categoria`, `curp`, `sexo`, `correop`, `correoi`, `contra`, `numcel`, `direccion`, `caracteristicas`) VALUES
(8, 2147483647, 'hgfhgvjmfg', 'vfcfcvcbvcbvcbvc', 'thytrytrytry', 'fdsdssdsfds', 'fdsfdsfd', 'fdxsf', 'vcxgfx', 'gxgxt', 'gfchrd', 'hgcht');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `Numcolab` int(11) NOT NULL,
  `horaentrada` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`Numcolab`, `horaentrada`) VALUES
(16391039, '0000-00-00 00:00:00'),
(16391039, '11/12/2017 03:47:39 a. m.'),
(16391039, '11/12/2017 03:48:46 a. m.'),
(16391039, '11/12/2017 03:53:00 a. m.'),
(16391039, '24/01/2018 07:27:57 a. m.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horariosalida`
--

CREATE TABLE `horariosalida` (
  `Numcolab` int(11) NOT NULL,
  `horasalida` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horariosalida`
--

INSERT INTO `horariosalida` (`Numcolab`, `horasalida`) VALUES
(16391039, '11/12/2017 03:53:22 a. m.'),
(16391039, '24/01/2018 07:28:04 a. m.');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`idEmpleado`);
ALTER TABLE `empleados` ADD FULLTEXT KEY `contraseña` (`contra`);
ALTER TABLE `empleados` ADD FULLTEXT KEY `contraseña_2` (`contra`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `idEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
