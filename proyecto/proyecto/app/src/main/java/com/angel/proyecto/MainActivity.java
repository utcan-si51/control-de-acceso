package com.angel.proyecto;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnacceder = (Button) findViewById(R.id.btnacceder);

        btnacceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast ---> CONTEXT, MENSAJE, DURACION
                Toast.makeText(MainActivity.this, "Mostrando un mensaje con Toast", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(MainActivity.this, login.class);
                startActivityForResult(i,0) ;

            }
        });

    }
}