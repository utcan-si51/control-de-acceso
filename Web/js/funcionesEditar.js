function presionEditar(e){
    e.preventDefault();
    var url = e.target.getAttribute('href');
    cargarEditar(url);
}

var conexion1;

function cargarEditar(url){
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarEditar;
    conexion1.open("GET", url, true);
    conexion1.send();
}
function procesarEditar(){
    var detalles = document.getElementById('fEditar');
    document.getElementById('tabla').style.display="none";
    document.getElementById('formulario_agregar').style.display="none";
    document.getElementById('formulario_editar').style.display="block";
    if(conexion1.readyState == 4){
        detalles.innerHTML = conexion1.responseText;
        var forEdit = document.getElementById('fEditar');
        forEdit.addEventListener('submit', presionForEdit, false);
    }
    else{
        detalles.innerHTML = 'Cargando...';
    }
}


/////////////////////////////////////////////////////////////////

function presionForEdit(e){
    e.preventDefault();
    var url = e.target.getAttribute('action');
    var url2 = e.target.getAttribute('value');
    enviarForEdit(url);
    cargarTabla(url2);
}



var conexion_ed;

function enviarForEdit(url){
 datos = obtenerDatosForEdit();
 conexion_ed = new XMLHttpRequest();
 conexion_ed.onreadystatechange = procesarForEdit;
 conexion_ed.open("POST", url, true);
    conexion_ed.setRequestHeader("Content-type","application/x-www-form-urlencoded");//determnar encabezado de formulario//
    conexion_ed.send(datos);
}


function obtenerDatosForEdit(){
    var data = "";
   var dato1 = document.getElementById('NumcolabE').value;
	var dato2 = document.getElementById('nombreE').value;
	var dato3= document.getElementById('categoriaE').value;
	var dato4 = document.getElementById('curpE').value;
	var dato5 = document.getElementById('sexoE').value;
	var dato6 = document.getElementById('correopE').value;
	var dato7 = document.getElementById('correoiE').value;
	var dato8 = document.getElementById('contraE').value;
	var dato9 = document.getElementById('numcelE').value;
	var dato10 = document.getElementById('direccionE').value;
	var dato11 = document.getElementById('caracteristicasE').value;
	var dato12 = document.getElementById('editar').value;
	var dato13 = document.getElementById('idEditar').value;
    data = 	"idEditar="+encodeURIComponent(dato13)+
			"&NumcolabE="+encodeURIComponent(dato1)+
			"&nombreE="+encodeURIComponent(dato2)+
			"&categoriaE="+encodeURIComponent(dato3)+
			"&curpE="+encodeURIComponent(dato4)+
			"&sexoE="+encodeURIComponent(dato5)+
			"&correopE="+encodeURIComponent(dato6)+
			"&correoiE="+encodeURIComponent(dato7)+
			"&contraE="+encodeURIComponent(dato8)+
			"&numcelE="+encodeURIComponent(dato9)+
			"&direccionE="+encodeURIComponent(dato10)+
			"&caracteristicasE="+encodeURIComponent(dato11)+
			"&editar="+encodeURIComponent(dato12);
    return data;
}



function procesarForEdit(){
    if(conexion_ed.readyState == 4){
        var c_nc = document.getElementById('NumcolabE').value = "";
		var c_n = document.getElementById('nombreE').value = "";
		var C_ca= document.getElementById('categoriaE').value = "";
		var c_cu = document.getElementById('curpE').value = "";
		var c_se = document.getElementById('sexoE').value = "";
		var c_cop = document.getElementById('correopE').value = "";
		var c_coi = document.getElementById('correoiE').value = "";
		var c_con = document.getElementById('contraE').value = "";
		var c_num = document.getElementById('numcelE').value = "";
		var c_dir = document.getElementById('direccionE').value = "";
		var c_cara = document.getElementById('caracteristicasE').value = "";
        document.getElementById('formulario_editar').style.display="none";
        document.getElementById('formulario_agregar').style.display="block";
    }
    else{
        detalless.innerHTML = 'Cargando...';
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
var conex;

function cargarTabla(url){
    conex = new XMLHttpRequest();
    conex.onreadystatechange = procesarTabla;
    conex.open("GET", url, true);
    conex.send();
}
function procesarTabla(){
    document.getElementById('tabla').style.display="block";
    var detalles = document.getElementById('cuerpo_tabla');
    if(conex.readyState == 4){
        detalles.innerHTML = conex.responseText;
    }
    else{
        detalles.innerHTML = 'Cargando...';
    }
}