addEventListener('mouseup', iniciarEliminar, false); 

function iniciarEliminar(){

   var cantidad = document.getElementById('totalFilas').rows.length;
   for (var f = 1; f <= cantidad-1; f++){
        var ob = document.getElementById('formel'+f);
        ob.addEventListener('submit', presionEliminar, false);

        var obedit = document.getElementById('btnEditar'+f);
        obedit.addEventListener('click', presionEditar, false);
    }
   
}

function presionEliminar(e){
    e.preventDefault();
    var url = e.target.getAttribute('action');
    var numero = e.target.getAttribute('value');
    enviarEliminar(url,numero);
}

var conexion_el;

function enviarEliminar(url,numero){
    var datos = obtenerDatosEliminar(numero);
    conexion_el = new XMLHttpRequest();
    conexion_el.onreadystatechange = procesarEliminar;
    conexion_el.open("POST", url, true);
    conexion_el.setRequestHeader("Content-type","application/x-www-form-urlencoded");//determnar encabezado de formulario//
    conexion_el.send(datos);
}

function obtenerDatosEliminar(numero){
    var data = "";
    var dato1 = document.getElementById('eliminar'+numero).value;
    var dato2 = document.getElementById('idEliminar'+numero).value;
    data = "eliminar="+encodeURIComponent(dato1)+"&idEliminar="+encodeURIComponent(dato2);
    return data;
}



function procesarEliminar(){
    var detalles = document.getElementById('cuerpo_tabla');
    if(conexion_el.readyState == 4){
        detalles.innerHTML = conexion_el.responseText;
    }
    else{
        detalles.innerHTML = 'Cargando...';
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






