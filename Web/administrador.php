<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>CONTROL DE ACCESO</title>
	<link rel="stylesheet" type="text/css" href="css/stylo.css">
	<script type="text/javascript" src="js/funciones.js" ></script>
	<script type="text/javascript" src="js/funcionesEditar.js" ></script>
	<script type="text/javascript" src="js/funcionEliminar.js" ></script>

</head>
<body>
	<ul id="menu">
		<li><a class="active" id="enlace1" >EMPLEADOS</a></li>
		<a class="pestaña" href="principal.php">SALIR</a>
	</ul>
	<br>

<div id="contenido" style="text-align: center;">
	<div id="formulario" style="text-align: center;">
		
		<div id="formulario_editar" style="display: none">
			<h3>EDITAR EMPLEADO </h3><br>
			<form method="post" id="fEditar" action="php/editar.php" value="php/tabla.php" style="width: 90%;margin-right: auto;margin-left: auto;">
			</form>
		</div>

		<div id="formulario_agregar">
		<h3>AGREGAR EMPLEADO</h3><br>
		<form method="post" id="fAgregar" action="php/agregar.php" style="width: 90%;margin-right: auto;margin-left: auto;">
			<input type="text" id="Numcolab" name="Numcolab" placeholder="Ingrese el N° de colaborador" required=""/> <br /><br />
			<input type="text" id="nombre" name="nombre" placeholder="Nombre del empleado" required=""><br><br>
			<input type="text" id="categoria" name="categoria" placeholder="Ingrese la categoria" required=""/> <br /><br />
			<input type="text" id="curp" name="curp" placeholder="Ingrese la curp" required=""/> <br /><br />
			<input type="text" id="sexo" name="sexo" placeholder="Ingrese el sexo" required=""/> <br /><br />
			<input type="text" id="correop" name="correop" placeholder="Ingrese el correo personal" required=""/> <br /><br />
			<input type="text" id="correoi" name="correoi" placeholder="Ingrese el correo institucional" required=""/> <br /><br />
			<input type="text" id="contra" name="contra" placeholder="Ingrese la contraseña" required=""/> <br />	<br />
			<input type="text" id="numcel" name="numcel" placeholder="Ingrese el numero celular" required=""/> <br /><br />
			<input type="text" id="direccion" name="direccion" placeholder="Ingrese su domicilio" required=""/> <br /><br />
			<input type="text" id="caracteristicas" name="caracteristicas" placeholder="Ingrese las caracterstias del empleado" required=""/> <br /><br />
			
			
			<input style="display: none" id="agregar" name="agregar" value="agregar">
			<button type="submit" style="background: green;padding: 10px;color: white;border-radius: 10px">Agregar</button>
		</form>
		</div>

	</div>
	<br>
	<div id="tabla">
		<table id="totalFilas" FRAME="void" RULES="rows" style="margin-right: auto;margin-left: auto;width: 90%;border-color: white">
              <thead style="background: #263238;color: #64DD17;font-weight: bold" >
                <tr style="text-align: center">
				  <th style="padding: 10px">ID EMPLEADO</th>
                  <th style="padding: 10px">N° COLABORADOR</th>
                  <th style="padding: 10px">NOMBRE</th>
                  <th style="padding: 10px">CATEGORIA</th>
                  <th style="padding: 10px">CURP</th>
                  <th style="padding: 10px">SEXO</th>
				  <th style="padding: 10px">CORREO PERSONAL</th>
                  <th style="padding: 10px">CORREO INSTITUCIONAL </th>
				  <th style="padding: 10px">CONTRASEÑA</th>
				  <th style="padding: 10px">NUMERO CELULAR</th>
				  <th style="padding: 10px">DIRECCIÓN</th>
				  <th style="padding: 10px">CARACTERISTRICAS</th>
				  
                </tr>
              </thead>
              <tbody id="cuerpo_tabla" style="text-align: center;">
              	<?php 
              		include('php/agregar.php');
              	?>
              </tbody>
        </table>
              <br>
	</div>
	<br>
</div>
	
</body>
</html>